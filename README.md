# SubReddit Viewer
Deployed version - https://igorrusso.gitlab.io/subreddit-viewer/

## How to launch
0. The computer with installed latest stable nodejs and npm is required.
1. Install angular cli - `npm install -g @angular/cli`.
2. Clone the repo and cd into its folder.
3. Run `npm install` and then `ng serve --open`.

## What was done
### Home page (the one with search field and posts list)
1. Search field accepts any string, and the app tries to get the reddit of that name. If it doesn't exist or special permissions are required - the error message is shown.

    1. The search is triggered either by pressing enter or clicking "View"-button.

2. The list of posts is shown, with the required fields - title, author, date of creation, comments number, score. The permalink is shown as icon. The thumb is shown.
3. Clicking on the title of the post in the posts lists view redirects user to the post details page.

### Post details page
1. The self_text, title of the post are shown.
2. If post doesn't have the image, no image is shown.
3. If post contains the video, the thumb of the video is shown - video player capabilities are not implemented.
4. The comments are shown in the tree-like hierarchical manner, showing the comment author and comment body text. But clicking on **More...** button and loading more data in the comments section is not implemented.

## Disclaimers and special points
While working on this app, some trade-offs were taken: 
1. No tests added. I even removed the test-related scripts, just to simplify the resulting codebase. But I understand that adding tests (unit, e2e) is essential part of real world software engineering.
2. There's a known issue with pagination - in some cases **NEXT** button is enabled when it ideally shouldn't be enabled.
3. Pagination position is reset when number of posts per page is changed. I'm not sure if it's a bug, but it may be confusing.
4. The folder structure may not be really typical for Angular, it just looked comfortable for me.
5. The posts list always skips *stickied*-posts, because their display logic is a bit out of the regular flow, and they show some rules of the current subreddit.
6. I didn't use the proper typings within the app - just to save some time. Normally, I would add typings everywhere it makes the smallest sense.
7. Some caching could be added. Now the data is being reused only on going back from post details to the list of posts.