import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class RedditAPIService {
  readonly BASE_URL: string = 'https://www.reddit.com';

  constructor(
    private http: HttpClient
  ) { }

  getPostDataById(postId: string) {
    return this.http.get(`${this.BASE_URL}/${postId}.json`).pipe(
      catchError(this.handleError({}))
    );
  }

  getSubRedditPosts(url) {
    return this.http.get(url).pipe(
      map(obj => {
        return obj['data'].children;
      }),
      catchError(this.handleError([]))
    );
  }

  public createSubRedditURL(name, limit, direction?, anchorPost?) {
    let url;

    if (direction && anchorPost) {
      let anchorId = `${anchorPost.kind}_${anchorPost.data.id}`;
      url = `${this.BASE_URL}/r/${name}.json?limit=${limit}&${direction}=${anchorId}`;
    }
    else {
      url = `${this.BASE_URL}/r/${name}.json?limit=${limit}`;
    }

    return url;
  }

  private handleError(defaultAnswer) {
    return (e): Observable<Object> => {
      // console.error(e);
      return of(defaultAnswer);
    };
  }
}