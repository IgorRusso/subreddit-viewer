import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';


import { AppComponent } from './app.component';
import { PostListComponent } from './home-page/post-list/post-list.component';
import { PostDetailsPageComponent } from './post-details-page/post-details-page.component';
import { AppService } from './app.service';
import { RedditAPIService } from './reddit-api.service';
import { HomePageComponent } from './home-page/home-page.component';
import { ControlPanelComponent } from './home-page/control-panel/control-panel.component';
import { CommentComponent } from './post-details-page/comment/comment.component';

@NgModule({
  imports: [
    BrowserModule,
    HttpClientModule,
    
    ReactiveFormsModule,
    RouterModule.forRoot([
      { path: '', component: HomePageComponent },
      {
        path: 'posts',
        children: [
          { path: '**', component: PostDetailsPageComponent }
        ]
      },
    ])
  ],
  declarations: [
    AppComponent,
    PostListComponent,
    PostDetailsPageComponent,
    HomePageComponent,
    ControlPanelComponent,
    CommentComponent
  ],
  bootstrap: [AppComponent],
  providers: [AppService, RedditAPIService]
})
export class AppModule { }