import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { RedditAPIService } from './reddit-api.service';

@Injectable({
  providedIn: 'root'
})
export class AppService {
  private postsPerPage: number;
  private postsPerPageOptions: number[] = [5, 10, 25];

  private searchTerm: string = '';
  private searchResult;
  private searchIndex = 0;

  private nextPageBlocked = false;

  private mainPageScrollPosition: [number, number];

  constructor(
    private api: RedditAPIService
  ) {
    this.postsPerPage = this.postsPerPageOptions[0];
  }

  getMainPageScrollPosition() {
    return this.mainPageScrollPosition;
  }

  setMainPageScrollPosition(position) {
    this.mainPageScrollPosition = position;
  }

  clearMainPageScrollPosition() {
    this.mainPageScrollPosition = null;
  }

  getSeachResult() {
    return this.searchResult;
  }

  getPostsPerPageOptions() {
    return this.postsPerPageOptions;
  }

  getPostsPerPage() {
    return this.postsPerPage;
  }

  setPostsPerPage(value) {
    this.postsPerPage = value;
    this.searchIndex = 0;
    this.nextPageBlocked = false;

    return this;
  }

  setSearchTerm(value) {
    this.searchTerm = value;
    this.searchIndex = 0;
    this.nextPageBlocked = false;

    return this;
  }

  getSearchTerm() {
    return this.searchTerm;
  }

  getPosts(direction?, anchorPost?) {
    const url = this.api.createSubRedditURL(this.searchTerm, this.postsPerPage, direction, anchorPost);

    return this.api.getSubRedditPosts(url).pipe(
      map(posts => posts.filter(d => !d.data.stickied)),
      map(posts => {

        // TODO I don't really like this block, but haven't manage to quickly find better solution.
        if (direction === 'after' && posts.length === 0) {
          this.nextPageBlocked = true;
          return this.searchResult;
        }

        this.searchResult = posts;

        if (direction === 'after') {
          this.searchIndex++;
        } else if (direction === 'before') {
          this.searchIndex--;
        }

        return this.searchResult;
      }),

    );
  }

  getNextPage() {
    if (this.searchResult && this.searchResult.length) {
      return this.getPosts('after', this.searchResult[this.searchResult.length - 1]);
    }
  }

  getPrevPage() {
    if (this.searchResult && this.searchResult.length) {
      return this.getPosts('before', this.searchResult[0]);
    }
  }

  isPrevAvailable() {
    return this.searchResult && this.searchIndex > 0;
  }

  isNextAvailable() {
    return !this.nextPageBlocked && this.searchResult && this.searchResult.length === this.postsPerPage;
  }

  getPost(id) {
    return this.api.getPostDataById(id);
  }
}