import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-comment',
  templateUrl: './comment.component.html',
  styleUrls: ['./comment.component.css']
})
export class CommentComponent implements OnInit {
  @Input() data;
  subComments;

  constructor() { }

  ngOnInit(): void {
    if(this.data.replies) {
      this.subComments = this.data.replies.data.children;
    }
  }

  moreClicked() {
    
  }
}
