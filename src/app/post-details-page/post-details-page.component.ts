import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AppService } from '../app.service';

@Component({
  selector: 'app-post-details-page',
  templateUrl: './post-details-page.component.html',
  styleUrls: ['./post-details-page.component.css']
})
export class PostDetailsPageComponent implements OnInit {
  post;
  postText: string;
  postImageUrl: string;

  comments;

  constructor(
    private route: ActivatedRoute,
    private postService: AppService
  ) { }

  ngOnInit() {
    let link = this.route.snapshot.pathFromRoot[2].url.join('/');

    this.postService.getPost(link).subscribe(d => {
      this.post = d[0].data.children[0].data;
      this.postText = this.post.selftext.replace(/\n/g, "<br />");
      this.postImageUrl = this.post.url.match(/(http(s)?:\/\/.*\.(?:png|jpg|jpeg|gif|bmp|tiff))/i) ? this.post.url : '';

      if (this.post.url.match(/(http(s)?:\/\/.*\.(?:png|jpg|jpeg|gif|bmp|tiff))/i)) {
        this.postImageUrl = this.post.url;
      } else if (this.post.thumbnail) {
        this.postImageUrl = this.post.thumbnail;
      } else {
        this.postImageUrl = '';
      }

      this.comments = d[1].data.children.map(rawComment => rawComment.data);
    });
  }
}