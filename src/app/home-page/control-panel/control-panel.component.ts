import { Component, EventEmitter, Output, OnInit, Input } from '@angular/core';
import { AppService } from 'src/app/app.service';

@Component({
  selector: 'app-control-panel',
  templateUrl: './control-panel.component.html',
  styleUrls: ['./control-panel.component.css']
})
export class ControlPanelComponent implements OnInit {

  @Output() searchRequested = new EventEmitter<string>();
  @Output() postsPerPageUpdated = new EventEmitter<number>();
  @Output() nextPageRequested = new EventEmitter<null>();
  @Output() prevPageRequested = new EventEmitter<null>();

  @Input() prevPageAvailable;
  @Input() nextPageAvailable;

  @Input() postsPerPageOptions;
  @Input() postsPerPage;

  constructor(public appService: AppService) {}

  ngOnInit() {
  }

  onViewClicked(searchTerm: string): void {
    if(searchTerm !== '') {
      this.searchRequested.emit(searchTerm);
    }
  }

  onPostsPerPageChanged(value) {
    this.postsPerPageUpdated.emit(+value);
    this.searchRequested.emit(this.appService.getSearchTerm());
  }

  onPrevPageClicked() {
    this.prevPageRequested.emit();
  }
  
  onNextPageClicked() {
    this.nextPageRequested.emit();
  }
}
