import { Component, OnInit } from '@angular/core';
import { AppService } from '../app.service';
import { ViewportScroller } from '@angular/common';
import { Router, NavigationStart } from '@angular/router';


@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html'
})
export class HomePageComponent implements OnInit {
  posts: [];
  subscription;

  constructor(
    public appService: AppService,
    private viewportScroller: ViewportScroller,
    private router: Router) {
  }

  ngOnInit() {
    this.posts = this.appService.getSeachResult();
    this.appService.isPrevAvailable();

    this.subscription = this.router.events.subscribe(event => {
      if (event instanceof NavigationStart) {
        this.appService.setMainPageScrollPosition([...this.viewportScroller.getScrollPosition()]);
      }
    });
  }

  ngAfterViewChecked() {
    if (this.appService.getMainPageScrollPosition()) {
      this.viewportScroller.scrollToPosition(this.appService.getMainPageScrollPosition());
      this.appService.clearMainPageScrollPosition();
    }
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
      this.subscription = null;
    }
  }

  searchForSubReddit(t: string): void {
    this.appService.setSearchTerm(t).getPosts().subscribe(postsData => {
      this.posts = postsData;
    });
  }

  onSearchRequested(e: string): void {
    if(e) {
      this.searchForSubReddit(e.trim());
    }
  }

  onPostsPerPageUpdated(value): void {
    this.appService.setPostsPerPage(value);
  }

  onNextPageRequested(): void {
    this.appService.getNextPage().subscribe(postsData => {
      this.posts = postsData;
    });
  }

  onPrevPageRequested(): void {
    this.appService.getPrevPage().subscribe(postsData => {
      this.posts = postsData;
    });
  }
}
